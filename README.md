# docker-netbox
This is the Netbox container running on a Debian latest.

Sample usage:  
```
docker run -d --name db --restart unless-stopped -p 5432:5432 --env ADMIN_USER=su --env ADMIN_PASSWORD='P@ssw0rd!' bsarda/postgresql:9.5
```
create in the database the user netbox, and the database netbox, then  
```
docker run -p 8443:8443 -d --name netbox bsarda/netbox --env SU_NAME=su --env SU_PASSWORD=SuP3Ruser! --env DB_SERVER=172.20.1.1 --env DB_PORT=45432 --env DB_NAME=netbox --env DB_USER=netbox --env DB_PASSWORD=netboxP@ss!  
docker logs -f netbox  
```
when you see "================= ready to use ! =================", then you can open a web browser on https://<yourserver>:8443


## Options as environment vars
**database access**  
- DB_SERVER => postgres server, default is 192.168.63.5  
- DB_PORT => postgres port, default is 5432
- DB_NAME => name of the database, default is netbox  
- DB_USER => database user, default is netbox  
- DB_PASSWORD => database user's password, default is P@ssw0rd!  

**superuser**
- SU_NAME => superuser login, default is admin  
- SU_MAIL => superuser mail, default is admin@localhost  
- SU_PASSWORD => superuser password, default is P@ssw0rd!  

**sslcert settings**
- SSL_COUNTRY=FR  
- SSL_STATE=IDF  
- SSL_CITY=Paris  
- SSL_ORG=Lab
