#!/bin/sh
docker ps -a | grep bsarda/netbox | awk '{print $1}' | xargs -n1 docker rm -f
docker rmi bsarda/netbox:latest
docker rmi bsarda/netbox:2.0.7
docker build --no-cache -t bsarda/netbox .
docker tag bsarda/netbox bsarda/netbox:latest
docker tag bsarda/netbox bsarda/netbox:2.0.7
