#!/bin/bash
touch /tmp/letitrun

if [ ! -f /var/netbox/initialized ]; then
  # stop apache2
  apache2ctl stop
  # init the file
  sed -i "s@'NAME':.*@'NAME':'$DB_NAME',@" /var/netbox/netbox/netbox/configuration.py
  sed -i "s@'USER':.*@'USER':'$DB_USER',@" /var/netbox/netbox/netbox/configuration.py
  password2=$(echo $DB_PASSWORD | sed 's/@/::at::/')
  sed -i "s@'PASSWORD':.*#.*@'PASSWORD':'$password2'@" /var/netbox/netbox/netbox/configuration.py
  sed -i "s/'PASSWORD':'$password2'/'PASSWORD':'$DB_PASSWORD',/" /var/netbox/netbox/netbox/configuration.py
  # host and port are passed on (first) exec time
  sed -i "s@'HOST': 'localhost'.*@'HOST':'$DB_SERVER',@" /var/netbox/netbox/netbox/configuration.py
  sed -i "s@'PORT':.*#.*@'PORT':'$DB_PORT',@" /var/netbox/netbox/netbox/configuration.py
  # open any!
  sed -i "s@^ALLOWED_HOSTS.*@ALLOWED_HOSTS = ['*']@" /var/netbox/netbox/netbox/configuration.py && \
  #sed -i "s/ALLOWED_HOSTS = \[\]/ALLOWED_HOSTS = \['$(hostname)'\, '$(hostname -I | sed 's@[[:space:]]@@' )', 'localhost', '127.0.0.1'\]/" /var/netbox/netbox/netbox/configuration.py
  # secret is per instance, not in dockerfile...
  sed -i "s@^SECRET_KEY.*@SECRET_KEY = '$(/var/netbox/netbox/generate_secret_key.py | sed 's/@/]/')'@" /var/netbox/netbox/netbox/configuration.py
  #
  # creating database
  /var/netbox/netbox/manage.py migrate
  # create superadmin
  #echo "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'admin')" | python /var/netbox/netbox/manage.py shell --plain
  echo "from django.contrib.auth.models import User; User.objects.create_superuser('$SU_NAME', '$SU_MAIL', '$SU_PASSWORD')" | python /var/netbox/netbox/manage.py shell --plain
  # create static
  /var/netbox/netbox/manage.py collectstatic
  # create cert
  mkdir /etc/ssl/server -p
  openssl req -x509 -nodes -days 3650 -newkey rsa:4096 -keyout /etc/ssl/server/server.key -out /etc/ssl/server/server.crt -subj "/C=$SSL_COUNTRY/ST=$SSL_STATE/L=$SSL_CITY/O=$SSL_ORG/CN=$(hostname)"
  # gunicorn conf
  cat <<- EOF > /var/netbox/gunicorn_config.py
command = '$(which gunicorn)'
pythonpath = '/var/netbox/netbox'
bind = '127.0.0.1:8001'
workers = 3
user = 'www-data'
EOF

  # create conf for apache
  # add listener
  echo "Listen 8443" >> /etc/apache2/ports.conf
  # create site with ssl
cat <<- EOF > /etc/apache2/sites-available/netboxssl.conf
<VirtualHost *:8443>
  ProxyPreserveHost On
  ServerName $(hostname)
  SSLEngine on
  SSLCertificateFile /etc/ssl/server/server.crt
  SSLCertificateKeyFile /etc/ssl/server/server.key
  SSLProtocol -all +TLSv1.2
  Alias /static /var/netbox/netbox/static
  WSGIPassAuthorization on
  <Directory /var/netbox/netbox/static>
    Options Indexes FollowSymLinks MultiViews
    AllowOverride None
    Require all granted
  </Directory>
  <Location /static>
    ProxyPass !
  </Location>
  ProxyPass / http://127.0.0.1:8001/
  ProxyPassReverse / http://127.0.0.1:8001/
</VirtualHost>
EOF
  # enable apache mods
  a2enmod ssl
  a2enmod wsgi
  a2enmod proxy
  a2enmod proxy_http
  # enable site
  rm /etc/apache2/sites-available/000-default.conf
  touch /etc/apache2/sites-available/000-default.conf
  #a2dissite 000-default
  a2ensite netboxssl
  # flag as initialized
  touch /var/netbox/initialized;
  echo "Initialized !"
fi

apache2ctl stop
# optionnal -- /var/netbox/netbox/manage.py loaddata initial_data
gunicorn -c /var/netbox/gunicorn_config.py netbox.wsgi &
# launch webserver
# python /var/netbox/netbox/manage.py runserver 0.0.0.0:8080 --insecure
apache2ctl start
# inform admin
echo "================= ready to use ! ================="

# wait in an infinite loop for keeping alive pid1
trap '/bin/sh -c "/usr/local/bin/stop.sh"' SIGTERM
while [ -f /tmp/letitrun ]; do sleep 1; done
exit 0;
