echo "WE'RE ABOUT TO STOP RIGHT NOW !"
# stop webserver
apache2ctl stop
# stop gunicorn
ps aux | grep root.*gunicorn | head -1 | awk '{print $2}'
kill $(ps aux | grep root.*gunicorn | head -1 | awk '{print $2}')
sleep 1
echo "Everything is properly stopped, we can exit"
# everything is properly stopped, we can exit
rm -f /opt/letitrun
